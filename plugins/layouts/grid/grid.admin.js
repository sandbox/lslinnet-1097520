
(function($) {

  Drupal.Panels = Drupal.Panels || {};
  Drupal.Panels.Grid = Drupal.Panels.Grid || {};

  /**
   * Basic show/hide functionality of the layouter.
   */
  Drupal.behaviors.PanelsGridAdmin = {
    'attach': function(context, setting) {
      // Show/hide layout manager button
      $('input#panels-grid-toggle-layout:not(.panels-grid-processed)', context)
        .addClass('panels-grid-processed')
        .click(function() {
          $('.panel-grid-admin')
            .toggleClass('panel-grid-no-edit-layout')
            .toggleClass('panel-grid-edit-layout');

          if ($('.panel-grid-admin').hasClass('panel-grid-edit-layout')) {
            $(this).val(Drupal.t('Hide layout designer'));
          }
          else {
            $(this).val(Drupal.t('Show layout designer'));
          }
          return false;
        });
    }
  };

  /**
   * Initialize the
   */
  Drupal.Panels.Grid = function () {

  }


})(jQuery);

/**
 * Provide an AJAX response command to allow the server to request
 * height fixing.
 */
//Drupal.CTools.AJAX.commands.grid_fix_height = function() {
//  Drupal.grid.fixHeight();
//};


/**
 * Provide an AJAX response command to fix the first/last bits of a
 * group.
 */
//Drupal.CTools.AJAX.commands.grid_fix_firstlast = function(data) {
//  console.log(data);
//  $(data.selector + ' > div > .' + data.base)
//    .removeClass(data.base + '-first')
//    .removeClass(data.base + '-last');
//
//  $(data.selector + ' > div > .' + data.base + ':first')
//    .addClass(data.base + '-first');
//  $(data.selector + ' > div > .' + data.base + ':last')
//    .addClass(data.base + '-last');
//};
