<?php

/**
 * Implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('Grid builder'),
  'category' => t('Builders'),
  'icon' => 'grid.png',
  'theme' => 'panels_grid',
  'admin theme' => 'panels_grid_admin',
  'css' => 'grid.css',
  'admin css' => 'grid.admin.css',
  'settings form' => 'panels_grid_settings_form',
  'settings submit' => 'panels_grid_settings_submit',
  'settings validate' => 'panels_grid_settings_validate',
  'regions function' => 'panels_grid_regions',
  'hook menu' => 'panels_grid_menu',

  // Reusable layout Builder specific directives
  'builder' => TRUE,
  'builder tab title' => 'Add Grid layout', // menu so translated elsewhere

  // defines how to fetch predefined/shared layouts
  'get child' => 'panels_grid_get_sublayout',
  'get children' => 'panels_grid_get_sublayouts',

  // Define ajax callbacks
  'ajax' => array(
    // Element specific callbacks.
    'element_settings' => 'panels_ajax_grid_edit_settings',
    //'element_add' => 'panels_ajax_grid_edit_add',
    'element_split' => 'panels_ajax_grid_edit_split',
    'element_remove' => 'panels_ajax_grid_edit_remove',
    'element_resize' => 'panels_ajax_grid_edit_resize',

    // Utility callbacks.
    'reuse' => 'panels_ajax_grid_edit_reuse',
    'container_settings' => 'panels_ajax_grid_edit_container',
  ),
);

/**
 * Merge the main grid plugin with a layout to create a sub plugin
 *
 * This is used for both panels_grid_get_sublayout and
 * panels_grid_get_sublayouts
 */
function panels_grid_merge_plugin($plugin, $layout) {
  $plugin['name'] = 'grid:' . $layout->name;
  $plugin['category'] = !empty($layout->category) ? check_plain($layout->category) : t('Miscellaneous');
  $plugin['title'] = check_plain($layout->admin_title);
  $plugin['description'] = check_plain($layout->admin_description);
  $plugin['layout'] = $layout;
  $plugin['builder'] = FALSE;
  $plugin['builder tab title'] = NULL;
  return $plugin;
}

/**
 * Callbacks to provide a single stored grid layout.
 */
function panels_grid_get_sublayout($plugin, $layout_name, $sublayout_name) {
  // Do not worry about caching; Panels is handling that for us.
  ctools_include('export');
  $item = ctools_export_crud_load('panels_layout', $sublayout_name);
  if ($item) {
    return panels_grid_merge_plugin($plugin, $item);
  }
}

/**
 * Callback to provide all stored grid layouts.
 */
function panels_grid_get_sublayouts($plugin, $layout_name) {
  $layouts[$layout_name] = $plugin;
  ctools_include('export');
  $items = ctools_export_load_object('panels_layout', 'conditions', array('plugin' => 'grid'));
  foreach ($items as $name => $item) {
    $layouts['grid:' . $name] = panels_grid_merge_plugin($plugin, $item);
  }
  return $layouts;
}

/**
 * Default settings if none are present.
 */
function panels_grid_default_settings(&$settings, &$layout) {
  // This indicates that this is a layout that they used the checkbox
  // on. The layout is still 'grid' but it's actually pointing
  // to another stored one and we have to load it.
  if (!empty($settings['shared_layout'])) {
    $layout = panels_get_layout('grid:' . $settings['shared_layout']);
  }

  if (!empty($layout['layout'])) {
    $settings = $layout['layout']->settings;
    return $settings;
  }

  /**
   * Need to specify how grids are defined.
   */
  if (empty($settings)) {
    // set up a default settings array.
    $settings = array(
      'grid_columns' => 12,
      'gutter_width' => 20,
      'column_width' => 60,
      'base class' => '',
      'alphaomega' => TRUE,
      'alphaomega_first_layer' => FALSE,
      /**
       * Layout structure
       */
//      'layout' => array(
//        'content' => array(
//          'content_aside' => array(),
//        ),
//        'aside' => array(),
//        'footer-a' => array(),
//        'footer-b' => array(),
//      ),
      'layout' => array(
        'content-a' => array(),
        'content-b' => array(),
        'content-c' => array(
          'content-d' => array(),
          'content-e' => array(),
        ),
      ),
      'regions' => array(
        'content-a' => array(
          'width' => 6,
          'region' => TRUE,
          'region_name' => 'content-a',
          'region_title' => 'Content A',
          'class' => '',
        ),
        'content-b' => array(
          'width' => 6,
          'region' => TRUE,
          'region_name' => 'content-b',
          'region_title' => 'Content B',
          'class' => '',
        ),
        'content-c' => array(
          'width' => 12,
          'region' => TRUE,
          'region_name' => 'content-c',
          'region_title' => 'Content C',
          'class' => '',
        ),
        'content-d' => array(
          'width' => 4,
          'region' => TRUE,
          'region_name' => 'content-d',
          'region_title' => 'Content D',
          'class' => '',
        ),
        'content-e' => array(
          'width' => 8,
          'region' => TRUE,
          'region_name' => 'content-e',
          'region_title' => 'Content E',
          'class' => '',
        ),
      ),
      /**
       * Layout element properties.
       */
//      'regions' => array(
//        'content' => array(
//          'width' => 8,
//          'region' => TRUE,
//          'region_name' => 'content',
//          'region_title' => 'Content',
//          'class' => '',
//        ),
//        'content_aside' => array(
//          'width' => 4,
//          'region' => TRUE,
//          'region_name' => 'content_aside',
//          'region_title' => 'Content Aside',
//          'class' => '',
//        ),
//        'aside' => array(
//          'width' => 4,
//          'region' => TRUE,
//          'region_name' => 'aside',
//          'region_title' => 'Aside',
//          'class' => '',
//        ),
//        'footer-a' => array(
//          'width' => 6,
//          'region' => TRUE,
//          'region_name' => 'footer-a',
//          'region_title' => 'Footer Left',
//          'class' => '',
//        ),
//        'footer-b' => array(
//          'width' => 6,
//          'region' => TRUE,
//          'region_name' => 'footer-b',
//          'region_title' => 'Footer Right',
//          'class' => '',
//        ),
//      ),
    );
  }
}
/**
 * Define the actual list of columns
 */
function panels_grid_regions($display, $settings, $layout) {
  $items = array();
  panels_grid_default_settings($settings, $layout);
  $items = panels_grid_get_regions($settings['regions']);
  return $items;
}

/**
 * Possible performance boost, calculate this when editing the grid.
 *
 * @param $layout
 *  The grid layout, from $settings['layout']
 * @return <array>
 *  List of valid regions in layout.
 */
function panels_grid_get_regions($regions) {
  static $static_regions = array();
  // @todo: consider static caching here.
  foreach ($regions AS $idx => $region) {
    if ($region['region']) {
      // @todo make sure region name is a machine readable name.
      $static_regions[$region['region_name']] = $region['region_title'];
    }
  }
  return $static_regions;
}

/**
 * Create a renderer object.
 *
 * The renderer object contains data that is passed around from function
 * to function allowing us to render our CSS and HTML easily.
 *
 * @todo Convert the functions to methods and make this properly OO.
 */
function panels_grid_create_renderer($admin, $css_id, $content, $settings, &$display, $layout, $handler) {
  $renderer = new stdClass;
  $renderer->settings = $settings;
  $renderer->content = $content;
  $renderer->css_id = $css_id;
  $renderer->did = &$display->did;
  if ($admin) {
    // do we need to do some special rendering when in admin mode?
  }
  $renderer->id_str = $css_id ? 'id="' . $css_id . '"' : '';
  $renderer->admin = $admin;
  $renderer->handler = $handler;

  $renderer->regions = $settings['regions'];

  // Set up basic classes for all of our components.
  $renderer->name = !empty($layout['layout']) ? $layout['layout']->name : $display->did;
  $renderer->base_class = $renderer->name;
  $renderer->item_class['region'] = 'panels-grid-region';
  $renderer->container = 'container-' . $settings['grid_columns'];
  $renderer->columns = $settings['grid_columns'];

  if (!$admin) {
    // Do we need additional information if we are NOT an admin?
  }

  if ($renderer->name != 'new') {
    // User v2 to guarantee all CSS gets regenerated to account for changes
    // hos some divs will be rendered.
    $renderer->css_cache_name = 'gridv2:' . $renderer->name;
    if ($admin) {
      ctools_include('css');
      ctools_css_clear($renderer->css_cache_name);
    }
  }
  return $renderer;
}


/**
 * Draw the grid!
 */
function theme_panels_grid($vars) {
  $css_id = $vars['css_id'];
  $content = $vars['content'];
  $settings = $vars['settings'];
  $display = $vars['display'];
  $layout = $vars['layout'];
  $handler = $vars['renderer'];

  panels_grid_default_settings($settings, $layout);

  $renderer = panels_grid_create_renderer(FALSE, $css_id, $content, $settings, $display, $layout, $handler);

  // Make sure grid css is generated.
  $css = panels_grid_render_css($renderer);

  if (!empty($renderer->css_cache_name) && empty($display->editing_layout)) {
    ctools_include('css');
    $filename = ctools_css_retrieve($renderer->css_cache_name);
    if (!$filename) {
      $filename = ctools_css_store($renderer->css_cache_name, $css, FALSE);
    }

    // Give the CSS to the renderer to put where it wants.
    if ($handler) {
      $handler->add_css($filename, 'module', 'all', FALSE);
    }
    else {
      drupal_add_css($filename);
    }
  }
  else {
    // If the id is 'new' we can't reliably cache the CSS in the filesystem
    // because the display does not truly exist, so we'll stick it in the
    // head tag. We also do this if we've been told we're in the layout
    // editor so that it always gets fresh CSS.
    drupal_add_css($css, array('type' => 'inline', 'preprocess' => FALSE));
  }

  // Do as flexible and store the CSS on the display in case the live
  // preview or something needs it.
  $display->add_css = $css;

  $output = '<div class="panel-grid ' . $renderer->base_class . ' ' . $renderer->container . ' clear-block" ' . $renderer->id_str . ">\n";

  panels_grid_prepare_layout($renderer);
  $output .= panels_grid_render_regions($renderer, $renderer->prepared['layout']);

  $output .= "\n</div>\n";

  return $output;
}

/**
 * Draw the grid layout.
 */
function theme_panels_grid_admin($vars) {
  $css_id = $vars['css_id'];
  $content = &$vars['content'];
  $settings = &$vars['settings'];
  $display = &$vars['display'];
  $layout = &$vars['layout'];
  $handler = &$vars['renderer'];

  // We never draw stored grid layouts in admin mode; they must be edited
  // from the stored layout UI at that point.
  if (!empty($layout['layout'])) {
    return theme_panels_grid(array(
      'css_id' => $css_id,
      'content' => $content,
      'settings' => $settings,
      'display' => $display,
      'layout' => $layout,
      'renderer' => $handler,
    ));
  }

  panels_grid_default_settings($settings, $layout);
  $renderer = panels_grid_create_renderer(TRUE, $css_id, $content, $settings, $display, $layout, $handler);

  $css = panels_grid_render_css($renderer);

  // For the administrative view, add CSS directly to head.
  drupal_add_css($css, array('type' => 'inline', 'preprocess' => FALSE));

  $output = '';
  $state_class = ' panel-grid-edit-layout';
  if (empty($display->editing_layout)) {
    $output .= '<input type="submit" id="panels-grid-toggle-layout" value ="' . t('Show layout designer') . '" />';
    if (user_access('administer panels layouts')) {
      //$output .= '<input type="hidden" class="panels-grid-reuse-layout-url" value="' . url($handler->get_url('layout', 'reuse'), array('absolute' => TRUE)) . '" />';
      //$output .= '<input type="submit" id="panels-grid-reuse-layout" class="ctools-use-modal" value ="' . t('Reuse layout') . '" />';
    }
    $state_class = ' panel-grid-no-edit-layout';
  }

  $output .= "<div class=\"panel-grid " . $renderer->container . " clear-block panel-grid-admin " . $state_class . "\" " . $renderer->id_str . ">\n";
  $output .= "<div class\"panel-grid-inside " . $renderer->container . "-inside\">\n";


  if ($renderer->admin) {
    $output .= _panels_grid_render_container_settings($renderer);
    $output .= '<div class="' . $renderer->container . '-replacer">';
  }

  // Make sure that we have prepared all data.
  panels_grid_prepare_layout($renderer);
  $output .= panels_grid_render_regions($renderer, $renderer->prepared['layout']);

  if ($renderer->admin) {
    $output .= '</div>';
  }

  $output .= "</div>\n</div>\n";

  drupal_add_js($layout['path'] . '/grid.admin.js');

  return $output;
}


/**
 * We need to prepared elements for rendering on the grid.
 * Every element need to be aware of their classes, in case
 * an element gets it width changed it requires a full grid
 * recalculation to figure out what elements should have
 * alpha and omega classes.
 */
function panels_grid_prepare_layout($renderer) {
  $layout = $renderer->settings['layout'];
  $columns = $renderer->columns;

  if (!isset($renderer->prepared)) {
    $renderer->prepared = array();

    if (!isset($renderer->prepared['layout'])) {
      $renderer->prepared['regions'] = array();
      $renderer->prepared['layout'] = _panels_grid_prepare_layout($renderer, $layout, $columns);
    }
  }
  return $renderer->prepared['layout'];
}

function _panels_grid_prepare_layout($renderer, $layout, $columns, $first = TRUE) {
  $regions = $renderer->regions;
  $remaining = $columns;
  $css_classes = array(
    'width' => 'grid-',
    'prefix' => 'prefix-',
    'suffix' => 'suffix-',
    'push' => 'push-',
    'pull' => 'pull-',
  );

  $items = array();

  foreach ($layout AS $id => $children) {
    $region = $regions[$id];
    $width = $region['width'];
    $prefix = !empty($region['prefix']) ? $region['prefix'] : 0;
    $suffix = !empty($region['suffix']) ? $region['suffix'] : 0;

    // The total width of gutter_width the current element will take up
    // is defined as prefix + width + suffix.
    $total_width = $prefix + $width + $suffix;

    // Figure out what css classes to put on the element.
    $classes = array();
    $classes[] = 'grid-element';
    $classes[] = 'grid-element-' . $id;
    if (!empty($region['class'])) {
      $classes[] = check_plain($region['class']);
    }

    foreach ($css_classes AS $key => $class) {
      if (isset($region[$key]) && $region[$key] != 0) {
        $classes[] = $class . check_plain($region[$key]);
      }
    }

    // If our currrent remaining is the same as the containerWidth
    // we presume that it's the first element.
    if ($columns == $remaining && !$first) {
      $classes[] = 'alpha';
    }

    if (($remaining - $total_width) == 0 && !$first) {
      $classes[] = 'omega';
    }

    if (!empty($children)) {
      $region['children'] = _panels_grid_prepare_layout($renderer, $children, $region['width'], FALSE);
      if (!empty($region['children'])) {
        $classes[] = 'clearfix';
        $classes[] = 'grid-region-has-children';
      }
    }

    $region['class'] = $classes;
    $renderer->prepared['regions'][$id] = $region;
    $items[$id] = $region;

    $remaining -= $total_width;
    if (($remaining) == 0) {
      $remaining = $columns;
    }
  }
  return $items;
}

/**
 * Render the supplied fragment of the grid.
 */
function panels_grid_render_regions($renderer, $regions) {
  $output = '';
  foreach ($regions AS $id => $children) {
    $output .= panels_grid_render_region($renderer, $id);
  }
  return $output;
}

/**
 * Render the specific grid element and it's children.
 */
function panels_grid_render_region($renderer, $id) {
  $region = $renderer->prepared['regions'][$id];
  $output = '<div class="' . implode($region['class'], ' ') . '">';
  if (!empty($renderer->admin)) {
    $output .= panels_grid_render_item_links($renderer, $id);
  }
  if (!empty($region['children'])) {
    $output .= panels_grid_render_regions($renderer, $region['children']);
  }
  if ($region['region']) {
    // Should this be wrapped in a html element?
    $output .= $renderer->content[$region['region_name']];
  }
  $output .= '</div>';
  return $output;
}

function panels_grid_render_css($renderer) {
  // Should generated the grid css here!
  $parent_class = $renderer->container;
  return panels_grid_render_css_group($renderer, $parent_class, $renderer->columns);
}

function panels_grid_render_css_group($renderer, $parent_class, $columns) {
  $column_width = $renderer->settings['column_width'];
  $gutter = $renderer->settings['gutter_width'];
  $margin = $gutter / 2;

  $css['.' . $parent_class] = array(
    'margin-left' => 'auto',
    'margin-right' => 'auto',
    'width' => (($columns * $column_width) + ($columns * $gutter)) . 'px',
  );

  $grid_elements = array();
  for ($i = 1; $i <= $columns; $i++) {
    $grid_elements[] = '.grid-' . $i;
  }

  $css[implode($grid_elements, ',')] = array(
    'display' => 'inline',
    'float' => 'left',
    'position' => 'relative',
    'margin-left' => $margin . 'px',
    'margin-right' => $margin . 'px',
  );

  $css['.alpha'] = array(
    'margin-left' => 0,
  );

  $css['.omega'] = array(
    'margin-right' => 0,
  );

  for ($i = 1; $i <= $columns; $i++) {
    $css['.' . $parent_class . ' .grid-' . $i] = array(
      'width' => (($i * $column_width) + ($i * $gutter) - $gutter) . 'px',
    );

    // Prefix
    if ($i - $columns != 0) {
      $width = ($i * $column_width) + ($i * $gutter);

      $css['.' . $parent_class . ' .prefix-' . $i] = array(
        'padding-left' => $width . 'px',
      );

      $css['.' . $parent_class . ' .suffix-' . $i] = array(
        'padding-right' => $width . 'px',
      );

      $css['.' . $parent_class . ' .push-' . $i] = array(
        'left' => $width . 'px',
      );

      $css['.' . $parent_class . ' .pull-' . $i] = array(
        'left' => '-' . $width . 'px',
      );
    }
  }

  ctools_include('css');
  return ctools_css_assemble($css);
}

/**
 * AJAX responder to edit grid settings for an item
 *
 * $handler object
 *   The display renderer handler object
 */
function panels_ajax_grid_edit_settings($handler, $id) {
  $settings = &$handler->display->layout_settings;
  panels_grid_default_settings($settings, $handler->plugins['layout']);

  // Need to check if element exists, if not render an error.
  if (empty($settings['regions'][$id])) {
    ctools_modal_render(t('Error'), t('Invalid item id.'));
  }

  $item = &$settings['regions'][$id];

  $title = t('Configure Grid element');

  $form_state = array(
    'display' => &$handler->display,
    'item' => &$item,
    'id' => $id,
    'settings' => &$settings,
    'ajax' => TRUE,
    'title' => $title,
    'op' => 'edit',
  );

  $output = ctools_modal_form_wrapper('panels_grid_config_item_form', $form_state);

  if (!empty($form_state['executed'])) {
    $output[] = panels_grid_rerender_grid($handler);
    $output[] = ctools_modal_command_dismiss();
  }

  $handler->commands = $output;
}


/**
 * Configure a row, column or region on the flexible page.
 *
 * @param <type> $form_state
 * @return <type>
 */
function panels_grid_config_item_form($form, &$form_state) {
  $display = &$form_state['display'];
  $item = &$form_state['item'];
  $settings = &$form_state['settings'];
  $id = &$form_state['id'];

  $form['class'] = array(
    '#title' => t('CSS class'),
    '#type' => 'textfield',
    '#default_value' => isset($item['class']) ? $item['class'] : '',
    '#description' => t('Enter a CSS class that will be used. This can be used to apply automatic styling from your theme, for example.'),
  );

  $options = range(1, $settings['grid_columns']); // fills an array with one value per column.
  $options = array_combine($options, $options); // we want 1 => 1 mapping not 0 => 1 mapping.

  $form['width'] = array(
    '#title' => t('Width'),
    '#type' => 'select',
    '#default_value' => isset($item['width']) ? $item['width'] : 1,
    '#options' => $options,
    '#description' => t('Select one of the avaible width'), // @todo: needs a better description.
  );

  $form['region'] = array(
    '#title' => t('Region'),
    '#type' => 'checkbox',
    '#default_value' => isset($item['region']) ? 1 : 0,
  );
  $form['region_title'] = array(
    '#title' => t('Region title'),
    '#type' => 'textfield',
    '#default_value' => isset($item['region_title']) ? $item['region_title'] : '',
    '#description' => t('Enter the Administrative title for this region'),
  );

  $form['region_name'] = array(
    //'#title' => t('Region name'),
    '#type' => 'hidden',
    '#value' => isset($item['region_name']) ? $item['region_name'] : '',
    //'#description' => t('The internal machine name for the region, may only contain Alphanumeric characters and underscores e.g. [a-z0-9_]'),
  );

  $extra_options = range(0, $settings['grid_columns'] - 1); // one less that max amount of columns.

  $form['prefix'] = array(
    '#title' => t('Prefix'),
    '#type' => 'select',
    '#default_value' => isset($item['prefix']) ? $item['prefix'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should be whitespace before grid element'),
  );

  $form['suffix'] = array(
    '#title' => t('Suffix'),
    '#type' => 'select',
    '#default_value' => isset($item['suffix']) ? $item['suffix'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should be whitespace after grid element.'),
  );

  $form['push'] = array(
    '#title' => t('Push'),
    '#type' => 'select',
    '#default_value' => isset($item['push']) ? $item['push'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should the grid element be pushed to the right?'),
  );

  $form['pull'] = array(
    '#title' => t('Pull'),
    '#type' => 'select',
    '#default_value' => isset($item['pull']) ? $item['pull'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should the grid element be pulled to the left'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function panels_grid_config_item_form_submit(&$form, &$form_state) {
  /**
   * If the item we are changing have children, we need to figure out if these
   * needs to be updated to fit within the grid elements width.
   * But we only want to do this if the grid element have actually changed size.
   */
  $settings = &$form_state['settings'];
  $item_props = array('width', 'region', 'region_title', 'region_name', 'prefix', 'suffix', 'push', 'pull', 'class');

  $current_region_name = $form_state['item']['region_name'];

  foreach ($item_props AS $property) {
    $form_state['item'][$property] = $form_state['values'][$property];
  }

  // Have the region name changed? if not just continue, else make sure there is not a doublet region name.
  if ($current_region_name != $form_state['item']['region_name']) {
    $name = preg_replace("/[^a-z0-9]/", '_', drupal_strtolower($form_state['item']['region_name']));
    while (isset($settings['elements'][$name])) {
      $name .= '_';
    }
    $form_state['item']['region_name'] = $name;
  }
}


function panels_grid_render_item_links($renderer, $id) {

  $title = t('Grid element');

  $links = array();
  $links[] = _panels_grid_modal_link(t('Settings'), $renderer->handler->get_url('layout', 'element_settings', $id));
  $links[] = _panels_grid_modal_link(t('Split element'), $renderer->handler->get_url('layout', 'element_split', $id));
  //$links[] = _panels_grid_modal_link(t('Prepend element'), $renderer->handler->get_url('layout', 'element_add', $id, 'prepend'));
  //$links[] = _panels_grid_modal_link(t('Append element'), $renderer->handler->get_url('layout', 'element_add', $id, 'append'));

  return theme('ctools_dropdown', array(
    'title' => $title,
    'links' => $links,
    'image' => FALSE,
    'class' => 'grid-layout-only grid-links grid-title grid-links-' . $id
  ));
}

/**
 * Simple wrapper for links elements for modal pop-ups.
 *
 * @param $title
 *    Translated title for the link.
 * @param $url
 *    the complete url for the link.
 * @return array
 *    link item array!
 */
function _panels_grid_modal_link($title, $url) {
  return array(
    'title' => $title,
    'href' => $url,
    'attributes' => array('class' => array('ctools-use-modal')),
  );
}

function panels_ajax_grid_edit_add($handler, $id, $location = 'prepend') {
  ctools_include('modal');
  ctools_include('ajax');
  $settings = &$handler->display->layout_settings;
  panels_grid_default_settings($settings, $handler->plugins['layout']);

  if (empty($settings['elements'][$id])) {
    ctools_modal_render(t('Error'), t('Invalid item id'));
  }

  // need to know the sibling we are placing this new element next to.
  $sibling = &$settings['elements'][$id];

  $title = ($location == 'after') ? t('Add new element after') : t('Add new element before');
  $item = array(
    'width' => 4,
    'region' => TRUE,
  );

  $form_state = array(
    'display' => &$handler->display,
    'parent' => &$parent,
    'item' => &$item,
    'id' => $id,
    'settings' => &$settings,
    'ajax' => TRUE,
    'title' => $title,
    'location' => $location,
  );

  $output = ctools_modal_form_wrapper('panels_grid_add_item_form', $form_state);
  if (!empty($form_state['executed'])) {

    panels_edit_cache_set($handler->cache);
    $output = array();
    $css_id = isset($handler->display->css_id) ? $handler->display->css_id : '';

    $renderer = panels_grid_create_renderer(TRUE, $css_id, array(), $settings,
                                            $handler->display, $handler->plugins['layout'], $handler);

    $content = '';
    if ($item['region'] == TRUE) {
      $handler->plugins['layout']['panels'][$form_state['key']] = $item['title'];

      $content = $handler->render_region($form_state['key'], array());

      // Manually add the hidden field that our region uses to store pane info.
      $content .= '<input type="hidden" name="panel[pane][' .
                  $form_state['key'] . ']" id="edit-panel-pane-' . $form_state['key'] . '" value="" />';
    }
    else {
      $content .= '';
    }

    // render the item
    $parent_class = 'grid-element-' . $id;
    $item_output = panels_grid_render_element($renderer, $item, $content, $form_state['key']);

    if ($location == 'after') {
      $output[] = ajax_command_prepend('#panels-dnd-main .' . $parent_class, $item_output);
    }
    else {
      $output[] = ajax_command_append('#panels-dnd-main .' . $parent_class, $item_output);
    }

    // Update the links of the parent? is this required?
    //$output[] = ajax_command_replace('.grid-links-' . $id, panels_grid_render_item_links($renderer, $id));

    $output[] = ctools_modal_command_dismiss();
  }

  $handler->commands = $output;
}


/**
 * Form to add a grid element to a grid layout
 * @param array
 *    $form_state
 * @return array
 *    An array describing a form for adding grid elements to a grid layout.
 */
function panels_grid_add_item_form(&$form, &$form_state) {
  $display = &$form_state['display'];
  $item = &$form_state['item'];
  $parent = &$form_state['parent'];
  $settings = &$form_state['settings'];
  $id = &$form_state['id'];

  $form['class'] = array(
    '#title' => t('CSS class'),
    '#type' => 'textfield',
    '#default_value' => isset($item['class']) ? $item['class'] : '',
    '#description' => t('Enter a CSS class that will be used. This can be used to apply automatic styling from your theme, for example.'),
  );

  $options = range(1, $settings['grid_columns']); // fills an array with one value per column.

  $form['width'] = array(
    '#title' => t('Width'),
    '#type' => 'select',
    '#default_value' => isset($item['width']) ? $item['width'] : 1,
    '#options' => $options,
    '#description' => t('Select one of the avaible width'), // @todo: needs a better description.
  );

  $form['region'] = array(
    '#title' => t('Region'),
    '#type' => 'checkbox',
    '#default_value' => isset($item['region']) ? 1 : 0,
  );

  $form['region_title'] = array(
    '#title' => t('Region title'),
    '#type' => 'textfield',
    '#default_value' => isset($item['region_title']) ? $item['region_title'] : '',
    '#description' => t('Enter the Administrative title for this region'),
  );

  $form['region_name'] = array(
    '#title' => t('Region name'),
    '#type' => 'machine_name',
    '#maxlength' => 21,
    '#default_value' => isset($item['region_name']) ? $item['region_name'] : '',
    '#machine_name' => array(
      'source' => array('region_title'),
      'exists' => 'panels_grid_region_exists'
    ),
  );

  $extra_options = range(0, $settings['grid_columns'] - 1); // one less that max amount of columns.

  $form['prefix'] = array(
    '#title' => t('Prefix'),
    '#type' => 'select',
    '#default_value' => isset($item['prefix']) ? $item['prefix'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should be whitespace before grid element'),
  );

  $form['suffix'] = array(
    '#title' => t('Suffix'),
    '#type' => 'select',
    '#default_value' => isset($item['suffix']) ? $item['suffix'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should be whitespace after grid element.'),
  );

  $form['push'] = array(
    '#title' => t('Push'),
    '#type' => 'select',
    '#default_value' => isset($item['push']) ? $item['push'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should the grid element be pushed to the right?'),
  );

  $form['pull'] = array(
    '#title' => t('Pull'),
    '#type' => 'select',
    '#default_value' => isset($item['pull']) ? $item['pull'] : 0,
    '#options' => $extra_options,
    '#description' => t('How many columns should the grid element be pulled to the left'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Utility function - takes a $handler object and builds the editing grid up.
 * used when rebuilding the layout.
 */
function panels_grid_rerender_grid($handler) {
  panels_edit_cache_set($handler->cache);
  $css_id = isset($handler->display->css_id) ? $handler->display->css_id : '';
  // On first ajax request the handler initialized correctly,
  // but on subrequests the  handler needs to be re-initialized.
  $handler->init($handler->plugin, $handler->display);
  $settings = $handler->display->layout_settings;
  // Make sure that each pane have been rendered.
  // @todo: verify that his is the most optimal way to do this!
  // We might have some problem with this when content have been placed into
  // the region.
  $content = array();
  foreach ($settings['elements'] AS $element_id => $element) {
    $content[$element['region_name']] = $handler->render_region($element['region_name'], array());
  }

  $renderer = panels_grid_create_renderer(TRUE, $css_id, $content, $handler->display->layout_settings,
                                          $handler->display, $handler->plugins['layout'], $handler);

  panels_grid_prepare_layout($renderer);
  $fragment = '';
  $fragment .= '<div class="' . $renderer->container . '-replacer">';
  $fragment .= panels_grid_render_regions($renderer, $renderer->settings['layout']);
  $fragment .= '</div>';
  $selector = '.' . $renderer->container . '-replacer';
  return ajax_command_replace($selector, $fragment);
}

function _panels_grid_render_container_settings($renderer) {
  $title = t('Container');
  $links = array();

  ctools_include('modal');
  ctools_include('ajax');

  $links[] = array(
    'title' => t('Settings'),
    'href' => $renderer->handler->get_url('layout', 'container_settings'),
    'attributes' => array('class' => 'ctools-use-modal'),
  );

  return theme('ctools_dropdown', array(
                                       'title' => $title,
                                       'links' => $links,
                                       'image' => FALSE,
                                       'class' => 'grid-layout-only grid-links grid-title',
                                  ));
}

function panels_ajax_grid_edit_container($handler, $id) {
  $title = t('Configure Grid container');

  $settings = &$handler->display->layout_settings;
  panels_grid_default_settings($settings, $handler->plugins['layout']);
  // Pass the handler into the form by reference so that it can be modified in
  // the submit function of the form.
  $form_state = array(
    'display' => &$handler->display,
    'settings' => &$settings,
    'ajax' => TRUE,
    'title' => $title,
    'op' => 'edit',
  );

  $output = ctools_modal_form_wrapper('panels_grid_container_settings_form', $form_state);
  if (!empty($form_state['executed'])) {
    // $form = $output;
    $output = array();
    // Save the information in the cache!
    panels_edit_cache_set($handler->cache);

    // We should update the viewed panel in order to reflect the new container settings.
    $output[] = panels_grid_rerender_grid($handler);
    $output[] = ctools_modal_command_dismiss();
  }
  $handler->commands = $output;
}


/**
 * The basic panel grid settings
 */
function panels_grid_container_settings_form(&$form, &$form_state) {
  $form = array();

  // Default values are based on 960.gs
  // 12 columns, 20 pixels gutter and 60 pixels column width
  $values = $form_state['settings'];
  $form['grid_columns'] = array(
    '#type' => 'textfield',
    '#title' => t('Grid columns'),
    '#description' => t('The number of avaible columns on the page.'),
    '#default_value' => isset($values['grid_columns']) ? $values['grid_columns'] : 12,
  );

  $form['column_width'] = array(
    '#type' => 'textfield',
    '#title' => t('A single columns width'),
    '#description' => t('How wide is each column in the grid to be in pixels?'),
    '#default_value' => isset($values['column_width']) ? $values['column_width'] : 60,
  );

  $form['gutter_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Gutter width'),
    // Only even numbers to avoid handling rounding errors when generating css.
    '#description' => t('How much space should there be between each grid column in pixels, only even numbers!'),
    '#default_value' => isset($values['gutter_width']) ? $values['gutter_width'] : 20,
  );

  $form['classes'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS Classes'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  $form['classes']['first_element'] = array(
    '#type' => 'textfield',
    '#title' => t('First element'),
    '#description' => t('The class of the first element in a row, in 960.gs it is called alpha'),
    '#default_value' => isset($values['classes']['first_element']) ? $values['classes']['first_element'] : 'alpha',
  );

  $form['classes']['last_element'] = array(
    '#type' => 'textfield',
    '#title' => t('Last element'),
    '#description' => t('The class of the last element in a row, in 960.gs it is called omega'),
    '#default_value' => isset($values['classes']['last_element']) ? $values['classes']['last_element'] : 'omega',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Impementation of settings submit callback
 */
function panels_grid_container_settings_form_submit(&$form, &$form_state) {
  /**
   * If the item we are changing have children, we need to figure out if these
   * needs to be updated to fit within the grid elements width.
   * But we only want to do this if the grid element have actually changed size.
   */
  $settings = &$form_state['settings'];
  $item_props = array(
    'grid_columns',
    'gutter_width',
    'column_width',
    'base class',
    'alphaomega',
    'alphaomega_first_layer',
    'classes',
  );

  $current_region_name = $form_state['item']['region_name'];

  foreach ($item_props AS $property) {
    $settings[$property] = $form_state['input'][$property];
  }

  // Have the region name changed? if not just continue, else make sure there is not dublet region name.
  if ($current_region_name != $form_state['item']['region_name']) {
    $name = preg_replace("/[^a-z0-9]/", '_', drupal_strtolower($form_state['item']['region_name']));
    while (isset($settings['elements'][$name])) {
      $name .= '_';
    }
    $form_state['item']['region_name'] = $name;
  }
}

/**
 * Implementation of settings validate callback
 *
 * @todo: should this be removed?
 */
function panels_grid_container_settings_form_validate($form, &$form_state) {

}

function panels_grid_region_exists($value, $element, $form_state) {
  return isset($form_state['settings']['elements'][$value]);
}


function panels_ajax_grid_edit_split($handler, $id, $location = 'prepend') {
  ctools_include('modal');
  ctools_include('ajax');

  $settings = &$handler->display->layout_settings;
  panels_grid_default_settings($settings, $handler->plugins['layout']);

  //dpm($settings);

  if (empty($settings['regions'][$id])) {
    ctools_modal_render(t('Error'), t('Invalid item id'));
    return;
  }

  if ($settings['regions'][$id]['width'] == 1) {
    ctools_modal_render(t('Error'), t('Unable to split an element with a width of one.'));
    return;
  }

  $form_state = array(
    'handler' => &$handler,
    'settings' => &$settings,
    'region' => &$settings['regions'][$id],
    'region_id' => $id,
    'ajax' => TRUE,
  );

  $output = ctools_modal_form_wrapper('panels_grid_split_element_form', $form_state);
  if (!empty($form_state['executed'])) {
    // should modify the selected grid element and add a new region next to it.

    $output[] = ctools_modal_command_dismiss();
  }

  $handler->commands = $output;
}

function panels_grid_split_element_form(&$form, &$form_state) {
  $form = array();

  $region = $form_state['region'];

  $directions = array(
    'vertical' => t('Vertical'),
    'horizontal' => t('Horizontal'),
  );

  $form['patterns'] = array(
    '#type' => 'radios',
    '#title' => t('Split direction:'),
    '#options' => $directions,
  );

  $regions = _panels_grid_split_element_valid($region);

  $form['new_regions'] = array(
    '#type' => 'select',
    '#title' => t('Number of regions'),
    '#options' => $regions,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function panels_grid_split_element_form_validate(&$form, &$form_state) {
  $region = $form_state['region'];
  // grid element should be larger than the amount of elements splitting it into.
  if ($form_state['new_regions'] > $region['width']) {
    form_error($form['new_regions'], t('Can not split region into more regions that there is columns.'));
  }
}

function panels_grid_split_element_form_submit(&$form, &$form_state) {
  // here we should add the new region(s)
  $settings = &$form_state['settings'];

  // Increment by one due to array index.
  $new_regions = $form_state['values']['new_regions'] + 1;

  $widths = $form_state['region']['width'] / ($new_regions + 1);
  dpm($widths, 'widths');
  $form_state['region']['region_name'];


  $settings['layout'];

  dpm($form_state['region'],'region');
  dpm($settings, 'layout settings');
  dpm($form, 'form');
  dpm($form_state, 'state');
}

function _panels_grid_split_element_valid($region) {
  $valid_splits = array();
  for ($i = 2; $i <= $region['width']; $i++) {
    if ($region['width'] % $i == 0) {
      $valid_splits[$i] = $i;
    }
  }
  return $valid_splits;
}
